ALTER Procedure [dbo].[DoctorDatabaseDelete]
	@DoctorID uniqueidentifier,
	@ErrorId int output,
	@ErrorDescription Varchar(4000) Output
AS
BEGIN
	BEGIN TRY
		Delete from DoctorDatabase 
		where DoctorID = @DoctorID
	END TRY
	BEGIN CATCH
		SET @ErrorDescription = Error_Message()
		SET @ErrorId = Error_Number()
	END CATCH
END
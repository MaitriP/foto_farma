﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FotoFarma.Common;
using System.Data;
using System.Collections;
using System.Web;
using FotoFarma.Model;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.IO;
using System.Data.OleDb;
using System.Globalization;
using ExcelDataReader;
using OfficeOpenXml;

namespace FotoFarma.Controllers
{
    public class WebController : ApiController
    {
        Connection ConnObj;
        DataSet ds;
        Hashtable hs;

        [HttpPost]
        public dynamic DrugDatabaseRetrieve()
        {
            ConnObj = new Connection();
            try
            {
                string AICCode = HttpContext.Current.Request.Form["AICCode"].ToString();
                string DeviceID = HttpContext.Current.Request.Form["DeviceID"].ToString();
                string Latitude = HttpContext.Current.Request.Form["Latitude"].ToString();
                string Longitude = HttpContext.Current.Request.Form["Longitude"].ToString();
                string Datetime = HttpContext.Current.Request.Form["Datetime"].ToString();

                ds = new DataSet();
                hs = new Hashtable();

                hs.Add("@AICCode", AICCode);
                hs.Add("@DeviceID", DeviceID);
                hs.Add("@Latitude", Latitude);
                hs.Add("@Longitude", Longitude);
                hs.Add("@Datetime", Datetime);
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spDrugDatabaseRetrieve);

                return ds.Tables[0];
            }
            catch (Exception e)
            {
                ConnObj.WriteError("DrugDatabaseRetrieve :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpGet]
        public dynamic DoctorDatabaseRetrieve()
        {
            ConnObj = new Connection();
            try
            {
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spDoctorDatabaseRetrieve);

                return ds.Tables[0];
            }
            catch (Exception e)
            {
                ConnObj.WriteError("DoctorDatabaseRetrieve :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        //3rd and final try with some modification in params and raw data input
        [HttpPost]
        public dynamic NegativeInteractionRetrieve([FromBody]List<InputModel> inputjson)
        {
            ConnObj = new Connection();
            try
            {
                ds = new DataSet();
                hs = new Hashtable();

                DataTable dt = new DataTable();
                dt.Columns.Add("CombinationID", typeof(string));

                for (int i = 0; i < inputjson.Count; i++)
                {
                    dt.Rows.Add(inputjson[i].ActivePrincipal);
                }
                hs.Add("@ActivePrincipleList", dt);
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spNegativeInteractionRetrieve);

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int t = 0; t < ds.Tables[0].Columns.Count; t++)
                        {
                            if (ds.Tables[0].Columns[t].Caption == "Flag")
                            {
                                ds.Tables[0].Columns.Remove(ds.Tables[0].Columns[t].Caption);
                            }
                        }
                        ds.Tables[0].Columns.Add("Family", typeof(string));

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            InputModel im = inputjson.Where(x => x.ActivePrincipal == ds.Tables[0].Rows[i][0].ToString()).FirstOrDefault();
                            string ErrorMessage = Strings.NegativeInteractionErrorMsg;
                            ErrorMessage = ErrorMessage.Replace(Strings.KeyFamilyOld.ToString(), im.FamilyOld.ToString());
                            ErrorMessage = ErrorMessage.Replace(Strings.KeyFamilyNew.ToString(), im.FamilyNew.ToString());
                            //ErrorMessage = ErrorMessage.Replace(Strings.KeyErrorMessage.ToString(), ds.Tables[0].Rows[i][1].ToString());
                            ds.Tables[0].Rows[i]["Family"] = ErrorMessage;
                        }
                        return new { Message = "", Data = ds.Tables[0] };
                    }
                    else
                    {
                        return new { Message = Strings.NegativeInteractionNoRecord.ToString(), Data = new DataTable() };
                    }
                }
                else
                {
                    return new { Message = Strings.NegativeInteractionNoRecord.ToString(), Data = new DataTable() };
                }
            }
            catch (Exception e)
            {
                ConnObj.WriteError("NegativeInteractionRetrieve :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpPost]
        public dynamic ValidateLogin([FromBody]LoginInput Json)
        {
            ConnObj = new Connection();
            try
            {
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@UserName", Json.UserName.ToString());
                hs.Add("@Password", Json.Password.ToString());
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spValidateLogin);
                if (ds.Tables[0].Rows.Count == 1)
                {
                    return new { ID = ds.Tables[0].Rows[0]["ID"].ToString(), UserName = ds.Tables[0].Rows[0]["UserName"].ToString(), Password = ds.Tables[0].Rows[0]["Password"].ToString() };
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = Strings.InvalidUserName });
                }
            }
            catch (Exception e)
            {
                ConnObj.WriteError("ValidateLogin :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpPost]
        public dynamic AcceptDisclaimer()
        {
            ConnObj = new Connection();
            try
            {
                string DeviceID = HttpContext.Current.Request.Form["DeviceID"].ToString();
                string AcceptDateTime = HttpContext.Current.Request.Form["AcceptDateTime"].ToString();
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@DeviceID", DeviceID.ToString());
                hs.Add("@AcceptDateTime", AcceptDateTime.ToString());
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");
                ds = ConnObj.GetData(hs, Strings.spDisclaimerInsert);

                return new { IsSuccess = true };
            }
            catch (Exception e)
            {
                ConnObj.WriteError("AcceptDisclaimer :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpPost]
        public dynamic ValidateLoginFormData()
        {
            ConnObj = new Connection();
            try
            {
                string UserName = HttpContext.Current.Request.Form["UserName"].ToString();
                string Password = HttpContext.Current.Request.Form["Password"].ToString();
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@UserName", UserName.ToString());
                hs.Add("@Password", Password.ToString());
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spValidateLogin);
                if (ds.Tables[0].Rows.Count == 1)
                {
                    return new { ID = ds.Tables[0].Rows[0]["ID"].ToString(), UserName = ds.Tables[0].Rows[0]["UserName"].ToString(), Password = ds.Tables[0].Rows[0]["Password"].ToString() };
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = Strings.InvalidUserName });
                }
            }
            catch (Exception e)
            {
                ConnObj.WriteError("DoctorDatabaseRetrieve :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpPost]
        public dynamic UserScanDataInsert()
        {
            ConnObj = new Connection();
            try
            {
                string AICCode = HttpContext.Current.Request.Form["AICCode"].ToString();
                string DeviceID = HttpContext.Current.Request.Form["DeviceID"].ToString();
                string Latitude = HttpContext.Current.Request.Form["Latitude"].ToString();
                string Longitude = HttpContext.Current.Request.Form["Longitude"].ToString();
                string Datetime = HttpContext.Current.Request.Form["Datetime"].ToString();
                int PurchaseType = Convert.ToInt16(HttpContext.Current.Request.Form["PurchaseType"].ToString());

                ds = new DataSet();
                hs = new Hashtable();

                hs.Add("@AICCode", AICCode);
                hs.Add("@DeviceID", DeviceID);
                hs.Add("@Latitude", Latitude);
                hs.Add("@Longitude", Longitude);
                hs.Add("@Datetime", Datetime);
                hs.Add("@PurchaseType", PurchaseType);                    
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spUserScanDataInsert);

                return new { IsSuccess = true };
            }
            catch (Exception e)
            {
                ConnObj.WriteError("UserScanDataInsert :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        #region "DoctorDatabase Project APIS"
        [HttpPost]
        public dynamic AddDoctor()
        {
            ConnObj = new Connection();
            try
            {
                string DoctorID = HttpContext.Current.Request.Form["DoctorID"].ToString();
                string DoctorName = HttpContext.Current.Request.Form["DoctorName"].ToString();
                string DoctorNumber = HttpContext.Current.Request.Form["DoctorNumber"].ToString();
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@DoctorName", DoctorName.ToString());
                hs.Add("@DoctorNumber", DoctorNumber.ToString());
                hs.Add("@DoctorID", DoctorID == "" ? Guid.Empty : new Guid(DoctorID.ToString()));
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spDoctorDatabaseInsert);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                ConnObj.WriteError("AddDoctor :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpPost]
        public dynamic DeleteDoctor()
        {
            ConnObj = new Connection();
            try
            {
                string DoctorID = HttpContext.Current.Request.Form["DoctorID"].ToString();
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@DoctorID", new Guid(DoctorID.ToString()));
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spDoctorDatabaseDelete);
                return new { IsSuccess = true };
            }
            catch (Exception e)
            {
                ConnObj.WriteError("DeleteDoctor :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }
        #endregion

        #region "InteractionDatabase Project APIs"
        [HttpPost]
        public dynamic UploadInteractionData()
        {
            ConnObj = new Connection();
            try
            {
                string JsonData = HttpContext.Current.Request.Form["JsonData"].ToString();
                List<Interaction> IntList = JsonConvert.DeserializeObject<List<Interaction>>(JsonData);
                ds = new DataSet();
                hs = new Hashtable();

                DataTable dt = new DataTable();
                dt.Columns.Add("FDI_T083", typeof(string));
                dt.Columns.Add("FDI_T086", typeof(string));
                dt.Columns.Add("Flag", typeof(string));
                DataRow dr;

                for (int i = 0; i < IntList.Count; i++)
                {
                    dr = dt.NewRow();
                    dr["FDI_T083"] = IntList[i].FDI_T083.ToString();
                    dr["FDI_T086"] = IntList[i].FDI_T086.ToString();
                    dr["Flag"] = IntList[i].Flag.ToString();
                    dt.Rows.Add(dr);
                }

                hs.Add("@InteractionDataList", dt);
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");
                ds = ConnObj.GetData(hs, Strings.spInteractionDatabaseProcess);
                ConnObj.WriteError("Success");
                return new { Count = ds.Tables[0].Rows[0][0].ToString() };
            }
            catch (Exception e)
            {
                ConnObj.WriteError("UploadInteractionData :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpGet]
        public dynamic GetInteractionData()
        {
            ConnObj = new Connection();
            try
            {
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spInteractionDatabaseRetrieve);

                return ds.Tables[0];
            }
            catch (Exception e)
            {
                ConnObj.WriteError("GetInteractionData :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpPost]
        public dynamic GetInteractionDataDownload()
        {
            ConnObj = new Connection();
            try
            {
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spInteractionDatabaseRetrieve);
                ConnObj.WriteError("Total Records from Interaction Db : " + ds.Tables[0].Rows.Count);

                #region "EPPlus" Excel Library
                var FilePath = new FileInfo(HttpContext.Current.Server.MapPath("~\\ExcelData\\InteractionData.xlsx"));
                if (File.Exists(FilePath.ToString()))
                    File.Delete(FilePath.ToString());

                using (var p = new ExcelPackage())
                {
                    p.Workbook.Worksheets.Add("IntegrationDataSheet");
                    var ws = p.Workbook.Worksheets["IntegrationDataSheet"];
                    DataTable dt = ds.Tables[0];//your datatable
                    for (int i = 1; i < dt.Columns.Count + 1; i++)
                    {
                        ws.Cells[1, i].Value = dt.Columns[i - 1].ColumnName;
                    }

                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        for (int k = 0; k < dt.Columns.Count; k++)
                        {
                            if (k == 0)
                            {
                                ws.Cells[j + 2, k + 1].Style.Numberformat.Format = "@";
                            }
                           
                                ws.Cells[j + 2, k + 1].Value = dt.Rows[j].ItemArray[k].ToString();
                            

                            //if (k == 0)
                            //{
                            //    ws.Cells[j + 2, k + 1].Style.Numberformat.Format = "@";
                            //}
                        }
                        //ConnObj.WriteError("cell " + j + 2);
                    }
                    p.SaveAs(new FileInfo(HttpContext.Current.Server.MapPath("~\\ExcelData\\InteractionData.xlsx")));

                }
                #endregion
                return ds.Tables[0].Rows.Count;
            }
            catch (Exception e)
            {
                ConnObj.WriteError("GetInteractionDataDownload :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpPost]
        public dynamic PostInteractionDataFile()
        {
            ConnObj = new Connection();
            try
            {
                string Path = HttpContext.Current.Server.MapPath("~//ExcelData//");

                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
                HttpPostedFile hpf = hfc[0];
                ConnObj.WriteError(hpf.FileName);

                string FileName = "InteractionData.xlsx";
                ConnObj.WriteError(Path + FileName);

                hpf.SaveAs(Path + FileName);
                //WriteFileFromStream(hpf.InputStream, Path + FileName);

                DataTable ExcelData = exceldata(HttpContext.Current.Server.MapPath("~//ExcelData//" + FileName));
                ExcelData = (from Datarow in ExcelData.AsEnumerable()
                             where Datarow.Field<string>("Flag").Trim() != "NA"
                             select Datarow).CopyToDataTable();

                DataTable dt = new DataTable();
                dt.Columns.Add("FDI_T083", typeof(string));
                dt.Columns.Add("FDI_T086", typeof(string));
                dt.Columns.Add("Flag", typeof(string));
                DataRow dr;

                for (int i = 0; i < ExcelData.Rows.Count; i++)
                {
                    dr = dt.NewRow();
                    dr["FDI_T083"] = ExcelData.Rows[i]["FDI_T083"].ToString().Trim();
                    dr["FDI_T086"] = ExcelData.Rows[i]["FDI_T086"].ToString();
                    dr["Flag"] = ExcelData.Rows[i]["Flag"].ToString();
                    dt.Rows.Add(dr);
                }
                hs = new Hashtable();
                hs.Add("@InteractionDataList", dt);
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");
                ds = ConnObj.GetData(hs, Strings.spInteractionDatabaseProcess);
                ConnObj.WriteError("Success");
                return new { Count = ds.Tables[0].Rows[0][0].ToString() };
            }
            catch (Exception e)
            {
                ConnObj.WriteError("PostInteractionDataFile :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }
        #endregion

        #region "DrugData Project APIs"
        [HttpPost]
        public dynamic UploadDrugData()
        {
            ConnObj = new Connection();
            try
            {
                string JsonData = HttpContext.Current.Request.Form["JsonData"].ToString();
                List<Drug> DrugList1 = JsonConvert.DeserializeObject<List<Drug>>(JsonData);
                List<Drug> DrugList = DrugList1.Where(x => x.Flag != "NA").ToList();
                ConnObj.WriteError("UploadDrugData : Total Records count to upload " + DrugList.Count);
                ds = new DataSet();
                hs = new Hashtable();

                DataTable dt = new DataTable();
                dt.Columns.Add("FDI_0001", typeof(string));
                dt.Columns.Add("FDI_1760", typeof(string));
                dt.Columns.Add("FDI_1761", typeof(string));
                dt.Columns.Add("FDI_1778", typeof(string));
                dt.Columns.Add("FDI_1764", typeof(string));
                dt.Columns.Add("FDI_1766", typeof(string));
                dt.Columns.Add("FDI_1765", typeof(string));
                dt.Columns.Add("FDI_1781", typeof(string));
                dt.Columns.Add("FDI_1767", typeof(string));
                dt.Columns.Add("FDI_1768", typeof(string));
                dt.Columns.Add("FDI_1771", typeof(string));
                dt.Columns.Add("FDI_1772", typeof(string));
                dt.Columns.Add("FDI_1769", typeof(string));
                dt.Columns.Add("FDI_0004", typeof(string));
                dt.Columns.Add("FDI_4875", typeof(string));
                dt.Columns.Add("FDI_0339", typeof(string));
                dt.Columns.Add("FDI_0370", typeof(string));
                dt.Columns.Add("FDI_T183", typeof(string));
                dt.Columns.Add("CLASS", typeof(int));
                dt.Columns.Add("UNITS", typeof(int));
                dt.Columns.Add("FAMILY", typeof(string));
                dt.Columns.Add("Flag", typeof(string));
                DataRow dr;

                for (int i = 0; i < DrugList.Count; i++)
                {
                    dr = dt.NewRow();
                    dr["FDI_0001"] = DrugList[i].FDI_0001.ToString();
                    dr["FDI_1760"] = DrugList[i].FDI_1760.ToString();
                    dr["FDI_1761"] = DrugList[i].FDI_1761.ToString();
                    dr["FDI_1778"] = DrugList[i].FDI_1778.ToString();
                    dr["FDI_1764"] = DrugList[i].FDI_1764.ToString();
                    dr["FDI_1766"] = DrugList[i].FDI_1766.ToString();
                    dr["FDI_1765"] = DrugList[i].FDI_1765.ToString();
                    dr["FDI_1781"] = DrugList[i].FDI_1781.ToString();
                    dr["FDI_1767"] = DrugList[i].FDI_1767.ToString();
                    dr["FDI_1768"] = DrugList[i].FDI_1768.ToString();
                    dr["FDI_1771"] = DrugList[i].FDI_1771.ToString();
                    dr["FDI_1772"] = DrugList[i].FDI_1772.ToString();
                    dr["FDI_1769"] = DrugList[i].FDI_1769.ToString();
                    dr["FDI_0004"] = DrugList[i].FDI_0004.ToString();
                    dr["FDI_4875"] = DrugList[i].FDI_4875.ToString();
                    dr["FDI_0339"] = DrugList[i].FDI_0339.ToString();
                    dr["FDI_0370"] = DrugList[i].FDI_0370.ToString();
                    dr["FDI_T183"] = DrugList[i].FDI_T183.ToString();
                    dr["CLASS"] = Convert.ToInt16(DrugList[i].CLASS.ToString());
                    dr["UNITS"] = Convert.ToInt16(DrugList[i].UNITS.ToString());
                    dr["FAMILY"] = DrugList[i].FAMILY.ToString();
                    dr["Flag"] = DrugList[i].Flag.ToString();
                    dt.Rows.Add(dr);
                }

                hs.Add("@DrugDataList", dt);
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");
                ds = ConnObj.GetData(hs, Strings.spDrugDatabaseProcess);
                ConnObj.WriteError("Success");
                return new { Count = ds.Tables[0].Rows[0][0].ToString() };
            }
            catch (Exception e)
            {
                ConnObj.WriteError("UploadDrugData :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        [HttpPost]
        public dynamic GetDrugDataDownload()
        {
            ConnObj = new Connection();
            try
            {
                ds = new DataSet();
                hs = new Hashtable();
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spDrugDatabaseRetrieveDownload);
                ConnObj.WriteError("Total Records from Db : " + ds.Tables[0].Rows.Count);

                #region "EPPlus" Excel Library
                var FilePath = new FileInfo(HttpContext.Current.Server.MapPath("~\\ExcelData\\DrugData.xlsx"));
                if (File.Exists(FilePath.ToString()))
                    File.Delete(FilePath.ToString());

                using (var p = new ExcelPackage())
                {
                    p.Workbook.Worksheets.Add("DrugDataSheet");
                    var ws = p.Workbook.Worksheets["DrugDataSheet"];
                   
                    DataTable dt = ds.Tables[0];//your datatable
                    for (int i = 1; i < dt.Columns.Count + 1; i++)
                    {
                        ws.Cells[1, i].Value = dt.Columns[i - 1].ColumnName;
                    }

                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        for (int k = 0; k < dt.Columns.Count; k++)
                        {
                            if (k == 0)
                            {
                                ws.Column(1).Style.Numberformat.Format = "@";
                            }
                            
                                ws.Cells[j + 2, k + 1].Value = dt.Rows[j].ItemArray[k].ToString();
                            
                        }
                        //ConnObj.WriteError("cell " + j + 2);
                    }
                    p.SaveAs(new FileInfo(HttpContext.Current.Server.MapPath("~\\ExcelData\\DrugData.xlsx")));

                }
                #endregion

                #region "Interop.Excel" Library
                //Microsoft.Office.Interop.Excel.Workbook MyBook = null;
                //Microsoft.Office.Interop.Excel.Application MyApp = null;
                //Microsoft.Office.Interop.Excel.Worksheet MySheet = null;

                //MyApp = new Microsoft.Office.Interop.Excel.Application();
                //MyApp.Visible = false;
                //MyApp.DisplayAlerts = false;
                //string FilePath = HttpContext.Current.Server.MapPath("~\\ExcelData\\DrugData1.xlsx");
                //MyBook = MyApp.Workbooks.Open(FilePath);
                //MySheet = MyBook.ActiveSheet;

                ////ConnObj.WriteError(MySheet.Name);
                //MySheet.Name = "DrugDatasheet";
                //MySheet.Cells.ClearContents();

                //DataTable dt = ds.Tables[0];//your datatable
                //for (int i = 1; i < dt.Columns.Count + 1; i++)
                //{
                //    MySheet.Cells[1, i] = dt.Columns[i - 1].ColumnName;                   
                //}

                //for (int j = 0; j < dt.Rows.Count; j++)
                //{
                //    for (int k = 0; k < dt.Columns.Count; k++)
                //    {
                //        MySheet.Cells[j + 2, k + 1] ="\t"+ dt.Rows[j].ItemArray[k];                        
                //    }
                //    ConnObj.WriteError("cell " + j + 2 );
                //}

                //MyBook.Save();
                //MyBook.Close();
                //MyApp.Quit();
                #endregion
                //return "Success"; 
                return ds.Tables[0].Rows.Count;
            }
            catch (Exception e)
            {
                ConnObj.WriteError("GetDrugDataDownload :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        //Sending a file from Angular Project. 
        [HttpPost]
        public dynamic PostDrugDataFile()
        {
            ConnObj = new Connection();
            try
            {
                string Path = HttpContext.Current.Server.MapPath("~//ExcelData//");

                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
                HttpPostedFile hpf = hfc[0];
                ConnObj.WriteError(hpf.FileName);

                string FileName = "DrugData.xlsx";
                ConnObj.WriteError(Path + FileName);

                hpf.SaveAs(Path + FileName);
                //WriteFileFromStream(hpf.InputStream, Path + FileName);

                DataTable ExcelData = exceldata(HttpContext.Current.Server.MapPath("~//ExcelData//" + FileName));
                ExcelData = (from Datarow in ExcelData.AsEnumerable()
                             where Datarow.Field<string>("Flag").Trim() != "NA"
                             select Datarow).CopyToDataTable();

                DataTable dt = new DataTable();
                dt.Columns.Add("FDI_0001", typeof(string));
                dt.Columns.Add("FDI_1760", typeof(string));
                dt.Columns.Add("FDI_1761", typeof(string));
                dt.Columns.Add("FDI_1778", typeof(string));
                dt.Columns.Add("FDI_1764", typeof(string));
                dt.Columns.Add("FDI_1766", typeof(string));
                dt.Columns.Add("FDI_1765", typeof(string));
                dt.Columns.Add("FDI_1781", typeof(string));
                dt.Columns.Add("FDI_1767", typeof(string));
                dt.Columns.Add("FDI_1768", typeof(string));
                dt.Columns.Add("FDI_1771", typeof(string));
                dt.Columns.Add("FDI_1772", typeof(string));
                dt.Columns.Add("FDI_1769", typeof(string));
                dt.Columns.Add("FDI_0004", typeof(string));
                dt.Columns.Add("FDI_4875", typeof(string));
                dt.Columns.Add("FDI_0339", typeof(string));
                dt.Columns.Add("FDI_0370", typeof(string));
                dt.Columns.Add("FDI_T183", typeof(string));
                dt.Columns.Add("CLASS", typeof(string));
                dt.Columns.Add("UNITS", typeof(int));
                dt.Columns.Add("FAMILY", typeof(string));
                dt.Columns.Add("Flag", typeof(string));
                DataRow dr;

                for (int i = 0; i < ExcelData.Rows.Count; i++)
                {
                    dr = dt.NewRow();
                    dr["FDI_0001"] = ExcelData.Rows[i]["FDI_0001"].ToString().Trim();
                    dr["FDI_1760"] = ExcelData.Rows[i]["FDI_1760"].ToString();
                    dr["FDI_1761"] = ExcelData.Rows[i]["FDI_1761"].ToString();
                    dr["FDI_1778"] = ExcelData.Rows[i]["FDI_1778"].ToString();
                    dr["FDI_1764"] = ExcelData.Rows[i]["FDI_1764"].ToString();
                    dr["FDI_1766"] = ExcelData.Rows[i]["FDI_1766"].ToString();
                    dr["FDI_1765"] = ExcelData.Rows[i]["FDI_1765"].ToString();
                    dr["FDI_1781"] = ExcelData.Rows[i]["FDI_1781"].ToString();
                    dr["FDI_1767"] = ExcelData.Rows[i]["FDI_1767"].ToString();
                    dr["FDI_1768"] = ExcelData.Rows[i]["FDI_1768"].ToString();
                    dr["FDI_1771"] = ExcelData.Rows[i]["FDI_1771"].ToString();
                    dr["FDI_1772"] = ExcelData.Rows[i]["FDI_1772"].ToString();
                    dr["FDI_1769"] = ExcelData.Rows[i]["FDI_1769"].ToString();
                    dr["FDI_0004"] = ExcelData.Rows[i]["FDI_0004"].ToString();
                    dr["FDI_4875"] = ExcelData.Rows[i]["FDI_4875"].ToString();
                    dr["FDI_0339"] = ExcelData.Rows[i]["FDI_0339"].ToString();
                    dr["FDI_0370"] = ExcelData.Rows[i]["FDI_0370"].ToString();
                    dr["FDI_T183"] = ExcelData.Rows[i]["FDI_T183"].ToString();
                    dr["CLASS"] = ExcelData.Rows[i]["CLASS"] != "" ? ExcelData.Rows[i]["CLASS"].ToString() : "";
                    dr["UNITS"] = Convert.ToInt16(ExcelData.Rows[i]["UNITS"].ToString());
                    dr["FAMILY"] = ExcelData.Rows[i]["FAMILY"] != "" ? ExcelData.Rows[i]["FAMILY"].ToString() : "";
                    dr["Flag"] = ExcelData.Rows[i]["Flag"] != "" ? ExcelData.Rows[i]["Flag"].ToString() : "I";
                    dt.Rows.Add(dr);
                }

                hs = new Hashtable();
                hs.Add("@DrugDataList", dt);
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");
                ds = ConnObj.GetData(hs, Strings.spDrugDatabaseProcess);
                ConnObj.WriteError("Success in PostDrugDataFile");
                return new { Count = ds.Tables[0].Rows[0][0].ToString() };
            }
            catch (Exception e)
            {
                ConnObj.WriteError("PostDrugDataFile :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        #endregion

        public static void WriteFileFromStream(Stream stream, string toFile)
        {
            using (FileStream fileToSave = new FileStream(toFile, FileMode.Create))
            {
                stream.CopyTo(fileToSave);
            }
        }

        public static DataTable exceldata(string filePath)
        {
            Connection ConnObj = new Connection();
            ConnObj.WriteError("ExcelData Function Path :" + filePath);
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
            ExcelDataReader.IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            DataSet result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                {
                    UseHeaderRow = true
                }
            });
            excelReader.Close();

            return result.Tables[0];
        }

        #region "Reports"
        [HttpPost]
        public dynamic UserScanReport()
        {
            ConnObj = new Connection();
            try
            {
                string DateFrom = HttpContext.Current.Request.Form["DateFrom"].ToString();
                string DateTo = HttpContext.Current.Request.Form["DateTo"].ToString();

                DateFrom = Convert.ToDateTime(DateFrom).ToString("MM/dd/yyyy hh:mm:ss");
                DateTo = Convert.ToDateTime(DateTo).ToString("MM/dd/yyyy hh:mm:ss");
                ConnObj.WriteError("User Scan Data DateFrom : " + DateFrom+ " DateTo : " + DateTo);
                
                ds = new DataSet();
                hs = new Hashtable();

                hs.Add("@DateFrom", DateFrom);
                hs.Add("@DateTo", DateTo);
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");
                ds = ConnObj.GetData(hs, Strings.spUserScanReport);

                return ds.Tables[0];
            }
            catch (Exception e)
            {
                ConnObj.WriteError("UserScanReport :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }
        
        [HttpPost]
        public dynamic UserAcceptanceReport()
        {
            ConnObj = new Connection();
            try
            {
                string DateFrom = HttpContext.Current.Request.Form["DateFrom"].ToString();
                string DateTo = HttpContext.Current.Request.Form["DateTo"].ToString();

                DateFrom = Convert.ToDateTime(DateFrom).ToString("MM/dd/yyyy hh:mm:ss");
                DateTo = Convert.ToDateTime(DateTo).ToString("MM/dd/yyyy hh:mm:ss");
                //ConnObj.WriteError("User Scan Data DateFrom : " + DateFrom + " DateTo : " + DateTo);
                ConnObj.WriteError("UserAcceptanceReport DateFrom : " + DateFrom + " DateTo : " + DateTo);

                ds = new DataSet();
                hs = new Hashtable();

                hs.Add("@DateFrom", DateFrom);
                hs.Add("@DateTo", DateTo);
                hs.Add("@ErrorId", 0);
                hs.Add("@ErrorDescription", "");

                ds = ConnObj.GetData(hs, Strings.spUserAcceptanceReport);

                return ds.Tables[0];
            }
            catch (Exception e)
            {
                ConnObj.WriteError("UserAcceptanceReport :" + e.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
            }
        }

        #endregion

        #region "NegativeInteractionTesting"
        //First Try With Get Method
        //[HttpGet]
        //public dynamic NegativeInteractionRetrieveGet(string Combination)
        //{
        //    ConnObj = new Connection();
        //    try
        //    {
        //        string[] ActiveCombination = Combination.Split(',');
        //        ds = new DataSet();
        //        hs = new Hashtable();

        //        DataTable dt = new DataTable();
        //        dt.Columns.Add("CombinationID", typeof(string));

        //        for (int i = 0; i < ActiveCombination.Length; i++)
        //        {
        //            dt.Rows.Add(ActiveCombination[i]);
        //        }
        //        hs.Add("@ActivePrincipleList", dt);
        //        hs.Add("@ErrorId", 0);
        //        hs.Add("@ErrorDescription", "");

        //        ds = ConnObj.GetData(hs, Strings.spNegativeInteractionRetrieve);

        //        return ds.Tables[0];
        //    }
        //    catch (Exception e)
        //    {
        //        ConnObj.WriteError("NegativeInteractionRetrieve :" + e.Message);
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
        //    }
        //}

        ////2nd try with Post Form Data
        //[HttpPost]
        //public dynamic NegativeInteractionRetrievePost()
        //{
        //    ConnObj = new Connection();
        //    try
        //    {
        //        string Combination = System.Web.HttpContext.Current.Request.Form["Combination"];
        //        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //        ListInput obj = new ListInput();
        //        obj = serializer.Deserialize<ListInput>(Combination);
        //        ds = new DataSet();
        //        hs = new Hashtable();

        //        DataTable dt = new DataTable();
        //        dt.Columns.Add("CombinationID", typeof(string));

        //        for (int i = 0; i < obj.Input.Count; i++)
        //        {
        //            dt.Rows.Add(obj.Input[i]);
        //        }
        //        hs.Add("@ActivePrincipleList", dt);
        //        hs.Add("@ErrorId", 0);
        //        hs.Add("@ErrorDescription", "");

        //        ds = ConnObj.GetData(hs, Strings.spNegativeInteractionRetrieve);

        //        return ds.Tables[0];
        //    }
        //    catch (Exception e)
        //    {
        //        ConnObj.WriteError("NegativeInteractionRetrieve :" + e.Message);
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new { ErrorCode = 101, ErrorMessage = e.Message });
        //    }
        //}
        #endregion
    }
    public class ListInput
    {
        public List<string> Input { get; set; }
    }
    public class InputJson
    {
        public string Inputjson { get; set; }
    }
}


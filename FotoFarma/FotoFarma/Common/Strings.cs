﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FotoFarma.Common
{
    public class Strings
    {
        #region "Messages"

        public static string NoRecords = "No record(s) found.";
        public static string InvalidUserName = "UserName/Pasword invalid.";
                
        public static string KeyFamilyOld = "[FamilyOld]";
        public static string KeyFamilyNew = "[FamilyNew]";
        public static string KeyErrorMessage = "[ErrorMessage]";
        public static string NegativeInteractionErrorMsg = "Fai attenzione: " + KeyFamilyOld + " assunto con " + KeyFamilyNew + " possono generare problemi. ";// + KeyErrorMessage + " Per maggiori informazioni consulta il tuo medico.";
        public static string NegativeInteractionNoRecord = "Il tuo farmaco è stato aggiunto, premi OK per procedere.";
        #endregion

        #region "Store Procedure Names"

        public static string spDrugDatabaseRetrieve = "DrugDatabaseRetrieve";
        public static string spNegativeInteractionRetrieve = "NegativeInteractionRetrieve";
        public static string spDoctorDatabaseRetrieve = "DoctorDatabaseRetrieve";
        public static string spValidateLogin = "ValidateLoginRetrieve";
        public static string spDoctorDatabaseInsert = "DoctorDatabaseInsert";
        public static string spDoctorDatabaseDelete = "DoctorDatabaseDelete";
        public static string spInteractionDatabaseProcess = "InteractionDatabaseProcess";
        public static string spInteractionDatabaseRetrieve = "InteractionDatabaseRetrieve";
        public static string spDrugDatabaseRetrieveDownload = "DrugDatabaseRetrieve_Download";
        public static string spDrugDatabaseProcess = "DrugDatabaseProcess";
        public static string spDisclaimerInsert = "DisclaimerInsert";
        public static string spUserScanReport = "UserScanReport";
        public static string spUserAcceptanceReport = "UserAcceptanceReport";
        public static string spUserScanDataInsert = "UserScanDataInsert";

        #endregion
    }
}
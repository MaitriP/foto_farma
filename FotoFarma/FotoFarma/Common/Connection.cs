﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FotoFarma.Common
{
    public class Connection
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["FotoFarma"].ConnectionString;
        public Connection()
        {
            
        }

        public static SqlConnection conn = null;
        public SqlDataAdapter adp;

        public DataSet GetData(Hashtable hs, string SpName)
        {
            DataSet ds = new DataSet();
            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();
                using (conn)
                {
                    SqlCommand cmd = new SqlCommand(SpName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (string param in hs.Keys)
                    {
                        cmd.Parameters.AddWithValue(param, hs[param]);
                    }
                    cmd.Parameters["@ErrorId"].Direction = ParameterDirection.Output;
                    cmd.Parameters["@ErrorDescription"].Direction = ParameterDirection.Output;
                    adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);
                }
                conn.Close();
            }
            catch (Exception e)
            {
                WriteError("\n GetData: " + SpName + " : " + e.Message);
            }
            return ds;
        }


        public void WriteError(string message)
        {
            string lines = "Message: " + message + ".\n Current Date is: " + System.DateTime.Now.ToString() + ". \n";
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            System.IO.StreamWriter file = System.IO.File.AppendText("LogFile.txt");
            file.WriteLine(lines);
            file.Close();
        }
    }
}
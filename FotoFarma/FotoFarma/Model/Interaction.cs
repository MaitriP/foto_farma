﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FotoFarma.Model
{
    public class Interaction
    {
        public string FDI_T083 { get; set; }
        public string FDI_T086 { get; set; }
        public string Flag { get; set; }
    }
}
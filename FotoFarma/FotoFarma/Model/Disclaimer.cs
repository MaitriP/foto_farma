﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FotoFarma.Model
{
    public class Disclaimer
    {
        public string DeviceID { get; set; }
        public DateTime AcceptDateTime { get; set; }
    }
}
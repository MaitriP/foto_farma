﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FotoFarma.Model
{
    public class InputModel
    {
        public string FamilyOld { get; set; }
        public string FamilyNew { get; set; }
        public string ActivePrincipal { get; set; }
    }
}
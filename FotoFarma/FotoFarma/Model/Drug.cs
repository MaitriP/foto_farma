﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FotoFarma.Model
{
    public class Drug
    {
        public string FDI_0001 { get; set; }
        public string FDI_1760 { get; set; }
        public string FDI_1761 { get; set; }
        public string FDI_1778 { get; set; }
        public string FDI_1764 { get; set; }
        public string FDI_1766 { get; set; }
        public string FDI_1765 { get; set; }
        public string FDI_1781 { get; set; }
        public string FDI_1767 { get; set; }
        public string FDI_1768 { get; set; }
        public string FDI_1771 { get; set; }
        public string FDI_1772 { get; set; }
        public string FDI_1769 { get; set; }
        public string FDI_0004 { get; set; }
        public string FDI_4875 { get; set; }
        public string FDI_0339 { get; set; }
        public string FDI_0370 { get; set; }
        public string FDI_T183 { get; set; }
        public int CLASS { get; set; }
        public int UNITS { get; set; }
        public string FAMILY { get; set; }
        public string Flag { get; set; }
    }
}
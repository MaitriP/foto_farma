﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FotoFarma.Model
{
    public class Doctor
    {
        public Guid ID { get; set; }
        public string DoctorName { get; set; }
        public string DoctorNumber { get; set; }
    }
}